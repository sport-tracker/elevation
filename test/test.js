const assert = require('chai').assert,
  elevation = require('../index')(__dirname+'/map.tif')
;

describe('elevation', () => {
  describe('#get()', () => {
    it('should return 135 with (54.6866, 25.2966)', () => {
      assert.equal(elevation.get(54.6866, 25.2966), 135);
    });

    it('should return \'\' with (undefined, 25.2966)', () => {
      assert.equal(elevation.get(undefined, 25.2966), '');
    });

    it('should throw a TypeError with (\'54.6866\', 25.2966)', () => {
      assert.throws(() => elevation.get('54.6866', 25.2966), TypeError, ' must be a number');
    });

    it('should throw out of range error with (190, 25.2966)', () => {
      assert.throws(() => elevation.get(190, 25.2966), Error, 'Access window out of range in RasterIO');
    });
  });
});
