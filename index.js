const gdal = require('gdal-next');

const elevation = {},
  wgs84 = gdal.SpatialReference.fromEPSG(4326);

let ds;

module.exports = path_geotif => {
  ds = gdal.open(path_geotif);
  return elevation;
};

elevation.get = (lat, lon) => {
  const coord_transform = new gdal.CoordinateTransformation(ds.srs, wgs84);
  const gt = ds.geoTransform;
  const dev = (gt[1] * gt[5] - gt[2] * gt[4]);
  const geo_transform_inv = [ gt[0], gt[5] / dev, -gt[2] / dev, gt[3], -gt[4] / dev, gt[1] / dev ];

  if (lat === undefined || lon === undefined) return '';

  const geo = coord_transform.transformPoint(lon, lat);

  const u = geo.x - geo_transform_inv[0];
  const v = geo.y - geo_transform_inv[3];

  const x = parseInt(geo_transform_inv[1] * u + geo_transform_inv[2] * v);
  const y = parseInt(geo_transform_inv[4] * u + geo_transform_inv[5] * v);

  return ds.bands.get(1).pixels.get(x, y);
};
