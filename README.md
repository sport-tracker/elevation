# elevation

[![Build Status](https://travis-ci.org/sport-tracker/elevation.svg?branch=master)](https://travis-ci.org/sport-tracker/elevation)
[![pipeline status](https://gitlab.com/sport-tracker/elevation/badges/master/pipeline.svg)](https://gitlab.com/sport-tracker/elevation/commits/master)

Installation:
```sh
npm i elevation
```

Installation using shared gdal library:
```sh
# requires gdal dev library (debian: sudo apt-get install libgdal-dev)
npm i elevation --build-from-source --shared_gdal
```

Usage:
```javascript
const elevation = require('elevation')('/path/to/geotif.tif');

elevation.get(latitude, longitude);
```
